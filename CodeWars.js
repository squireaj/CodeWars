//
//
// // The first century spans from the year 1 up to and including the year 100, The second from the year 101 up to and including the year 200, etc.
// //
// // Task: Given a year, return the century it is in.
//
// const century = function (date) {
//     return Math.ceil(date / 100)
// };
//
// const c = (d) => {return Math.ceil(d/100)};
//
// console.log(century(1));
// console.log(century(23));
// console.log(century(100));
// console.log(century(200));
// console.log(century(201));
//
// console.log("----------");
//
// console.log(c(1));
// console.log(c(23));
// console.log(c(100));
// console.log(c(200));
// console.log(c(201));
//
// // https://jsfiddle.net/squireaj/Lg8hp63u/3/
//
// // **********************************  6 kyu: Mexican Wave **********************************
//
// function wave(str){
//     const makeWave = (startingArray, returnArray, count) => {
//         if(count === startingArray.length) {
//             return returnArray
//         }else if(startingArray[count] === " "){
//             return makeWave(startingArray, returnArray, count+1)
//         }else{
//             let collect = [];
//             for(let i = 0; i < startingArray.length; i++){
//                 if(i === count){
//                     collect = [...collect, startingArray[i].toUpperCase()]
//                 }else {collect = [...collect, startingArray[i]]}
//             }
//             return makeWave(startingArray, [...returnArray, collect.join("")], count+1)
//         }
//     };
//     return makeWave(str.split(""), [], 0)
// }
//
// // **********************************  6 kyu: Mexican Wave **********************************
//
// function findOutlier(integers){
//     let first = Math.abs(integers[0]) % 2;
//     let second = Math.abs(integers[1]) % 2;
//     let third = Math.abs(integers[2]) % 2;
//
//     return first + second + third <= 1 ? integers.filter(n => Math.abs(n) % 2 === 1)[0] : integers.filter(n => Math.abs(n) % 2 === 0)[0]
// }
//
//
// // **********************************  7 kyu: Get the Middle Character **********************************
//
// function getMiddle(s)
// {
//     const arr = s.split("");
//     const mid = Math.floor(arr.length / 2) - 1;
//     return arr.length % 2 === 0 ? arr[mid] : `${arr[mid]}${arr[mid+1]}`
// }
//
// function getMiddle(s)
// {
//     const arr = s.split("");
//     const even = arr.length % 2 === 0;
//     let mid = even ? arr.length / 2 : Math.floor(arr.length / 2);
//     return even ? `${arr[mid-1]}${arr[mid]}` : arr[mid]
// }
// // **********************************   7kyu: Jaden Casing Strings **********************************
//
// String.prototype.toJadenCase = function () {
//     let string = this.split(" ");
//     let words = string.map(a => a.split(''));
//     let lower = words.map(a => a.map(a => a.toLowerCase()));
//     let cap = lower.map(a => [a[0].toUpperCase(), ...a].filter(e => a.indexOf(e) !== 0).join(''));
//     return finish = cap.join(' ');
// };
//
// let test = "How can mirrors be real if our eyes aren't real".toJadenCase();
// // console.log(test);
//
// // **********************************  6 kyu: Jewel Thief **********************************
//
// const crack = function(safe) {
//
//     for(const i = 0; i < 100; i++){
//         if(i<10){
//             if(safe.unlock(`L0${i}-R00-R00`).includes("click")){
//                 console.log("click");
//                 const first = `L0${i}`
//             }else if(safe.unlock(`R0${i}-R00-R00`).includes("click")){
//                 console.log("click");
//                 const first = `R0${i}`
//             }
//         }else{
//             if(safe.unlock(`L${i}-R00-R00`).includes("click")){
//                 console.log("click");
//                 const first = `L${i}`
//             }else if(safe.unlock(`R${i}-R00-R00`).includes("click")){
//                 console.log("click");
//                 const first = `R${i}`
//             }
//         }
//     }
//     console.log("first1: ", first);
//     for(const i = 0; i < 100; i++){
//         if(i<10){
//             if(safe.unlock(`${first}-L0${i}-L00`).includes("click-click")){
//                 const second = `${first}-L0${i}`
//             }else if(safe.unlock(`${first}-R0${i}-L00`).includes("click-click")){
//                 const second = `${first}-R0${i}`
//             }
//         }else{
//             if(safe.unlock(`${first}-L${i}-L00`).includes("click-click")){
//                 const second = `${first}-L${i}`
//             }else if(safe.unlock(`${first}-R${i}-L00`).includes("click-click")){
//                 const second = `${first}-R${i}`
//             }
//         }
//     }
//     console.log("second: ", second);
//     for(const i = 0; i < 100; i++){
//         if(i<10){
//             if(safe.unlock(`${second}-L0${i}`).includes("click-click-click")){
//                 const third = `${second}-L0${i}`
//             }
//             else if(safe.unlock(`${second}-R0${i}`).includes("click-click-click")){
//                 const third = `${second}-R0${i}`
//             }
//         }else{
//             if(safe.unlock(`${second}-L${i}`).includes("click-click-click")){
//                 const third = `${second}-L${i}`
//             }
//             else if(safe.unlock(`${second}-R${i}`).includes("click-click-click")){
//                 const third = `${second}-R${i}`
//             }
//         }
//     }
//     safe.unlock(third);
//     return safe.open();
// };
//
// // **********************************  n kyu: Number of trailing zeros N **********************************
// function zeros (n) {
//     let findZeros = function (count, num) {
//         if(num < 1){
//             return count
//         }else{
//             let newCount = count === 0 ? Math.floor(num/5) : count + Math.floor(num/5);
//            return findZeros(newCount, num/5)
//         }
//     };
//     return findZeros(0, n)
// }
//
// // **********************************  6 kyu: Not prime numbers **********************************
//
// function oddNotPrime(n){
//     const isPrime = num => {
//         for(let i = 2; i < num; i++)
//             if(num % i === 0) return false;
//         return num !== 1;
//     }
//
// }
//
// function notPrimes(a,b){
//     let arePrimes = () => {
//     console.log("Args: ", arguments);
//         let isPrime = (x) => {
//             for(let i = 2; i < x; i++){
//                 if(x % i === 0){
//                     return 0
//                 }
//             }
//             return 1
//         };
//         for(let i = 0; i < arguments.length; i++){
//             if(!isPrime(arguments[i])) return false
//         }
//         return true
//     };
//     let notEvenPrimes = function (x, y) {
//         let returnArr = [];
//         for(let i=x; i<y; i++){
//             let stringNums = i.toString().split('');
//             let nums = stringNums.map(a => parseInt(a));
//                 // console.log("Nums: ", nums);
//             if(arePrimes.bind(this).apply(null, nums)){
//                 console.log("Prime");
//                 returnArr = [i, ...returnArr]
//             }
//         }
//         return returnArr;
//     };
//     return notEvenPrimes(a, b)
// }
//
// notPrimes(2, 222);
//
// function notPrimes(a,b){
//     let returnArr = [];
//     for(let i=a; i<b; i++){
//         if(arePrimes((Math.floor(i/10)), (i%10))){
//             returnArr = [i, ...returnArr];
//         }
//     }
//     return returnArr;
//
// }
//
// function arePrimes(){
//     let isPrime = (x) => {
//         for(let i = 2; i < x; i++){
//             if(x % i === 0){
//                 return 0
//             }
//         }
//         return 1
//     };
//     for(let i = 0; i < arguments.length; i++){
//         if(!isPrime(arguments[i])) return false
//     }
//     return true
// }
//
// let args = [2,4,5,4,3];
// function test() {
//     return arguments
// }
// let working = test.apply(null, args);
//
//
//
// function arr(num) {
//     let stringNums = num.toString().split('');
//     let nums = stringNums.map(a => parseInt(a));
//     return nums
// }
//
//
// function notPrimes(a, b) {
//     const isPrime = num => {
//         for (let i = 2; i < num; i++) {
//             if (num % i === 0) {
//                 return false
//             }
//         }
//         return num > 1
//     };
//
//     const arePrimes = function(){
//         for(let i = 0; i < arguments.length; i++){
//             if(!isPrime(arguments[i])) return false
//         }
//         return true
//     };
//     let returnArr = [];
//     for (let i = a; i < b; i++) {
//         let stringNums = i.toString().split('');
//         let nums = stringNums.map(a => parseInt(a));
//         if (arePrimes.apply(null, nums)) {
//             console.log("Prime", nums);
//             returnArr = [...returnArr, i]
//         }
//     }
//     return returnArr.filter(a => !isPrime(a));
// }
//
// notPrimes(2, 222);
//
// // test.assert_equals(not_primes(2, 222), [22, 25, 27, 32, 33, 35, 52, 55, 57, 72, 75, 77]);
// // test.assert_equals(not_primes(2, 77), [22, 25, 27, 32, 33, 35, 52, 55, 57, 72, 75]);
// // test.assert_equals(not_primes(2700, 3000), [2722, 2723, 2725, 2727, 2732, 2733, 2735, 2737, 2752, 2755, 2757, 2772, 2773, 2775]);
// // test.assert_equals(not_primes(500, 999), [522, 525, 527, 532, 533, 535, 537, 552, 553, 555, 572, 573, 575, 722, 723, 725, 732, 735, 737, 752, 753, 755, 772, 775, 777]);
// // test.assert_equals(not_primes(999, 2500), [2222, 2223, 2225, 2227, 2232, 2233, 2235, 2252, 2253, 2255, 2257, 2272, 2275, 2277, 2322, 2323, 2325, 2327, 2332, 2335, 2337, 2352, 2353, 2355, 2372, 2373, 2375]);
//
// // missing("123567") = 4
// // missing("899091939495") = 92
// // missing("9899101102") = 100
// // missing("599600601602") = -1 -- no number missing
// // missing("8990919395") = -1 -- error in sequence. Both 92 and 94 missing.
//
// let nums = "10111213141617";
//
// let missing = function (nums) {
//     let findSequence = function (count, sp,  countBool, missingArr, aggregateArr) {
//             console.log(`---------------Ran ${count}-----------------`);
//             if(count * (aggregateArr.length) >= nums.length || count >= nums.length){
//                 return aggregateArr
//             }else if(countBool){
//                 console.log("stage 1: ", aggregateArr);
//                 return findSequence(count, count + count, true, [...aggregateArr, nums.substring(sp, count)] )
//             }else if(nums.substring(sp, count) - nums.substring(sp + count, sp+count+count) !== -1){
//                 console.log("stage 2:aggregateArr ", aggregateArr);
//                 console.log("stage 2:count ", count);
//                 console.log("stage 2:sp ", sp);
//                 console.log("stage 2:substring 1 ", nums.substring(sp, count));
//                 console.log("stage 2:substring 2 ", nums.substring(sp + count, sp+count+count));
//                 return findSequence(count === 0  ? count +1 : count*2, sp, false, [], [])
//             }else if(nums.substring(sp, count) - nums.substring(sp + count, sp+count+count) === -1){
//                 console.log("stage 3 aggregateArr: ", aggregateArr);
//                 console.log("stage 3 count: ", count);
//                 console.log("stage 3 sp: ", sp);
//                 console.log("stage 3:substring 1 ", nums.substring(sp, count));
//                 console.log("stage 3:substring 2 ", nums.substring(sp + count, sp+count+count));
//                 return findSequence(count, count, true, missingArr, [...aggregateArr, nums.substring(sp, count)] )
//             }
//     };
//     return findSequence(0, 0, false, [], [])
// };
//
// missing(nums);
//
//
// let nums = "10111213141617";
// let count = 2;
// let sp = 0;
//
// nums.substring(sp, count) - nums.substring(sp + count, sp+count+count)
//
// nums.substring(sp, count) - nums.substring(sp + count, sp+count+count)
//
//
//
// // **********************************  7 kyu: Automorphic Number (Special Numbers Series #6) **********************************
//
// function knockKnock(str){
//     const commonCharacter = function(){
//         const ar1 = arguments[0];
//         const ar2 = arguments[1];
//         if(ar2){
//             for(let c in ar1){
//                 for(let m in ar2){
//                     if(ar1[c] === ar2[m]){
//                         return ar1[c]
//                     }
//                 }
//             }
//         }
//     };
//
//     let rArray = ["!"];
//     const words = str.split(" ");
//     for (let i = 1; i<words.length; i++){
//         let cc = commonCharacter(words[i].split(''), (words[i-1]).split(''));
//         if(cc){
//             rArray = [...rArray, cc];
//         }else{
//             rArray = [...rArray, "^"]
//         }
//
//     }
//     return rArray.join('');
//
// }
//
// console.log("!k^vo^", knockKnock("vkxn kuxsrh adpvi vdbasjoj ypoi axwwe"));
// console.log("!uu^s^x9ey4aji^^d", knockKnock("9utthp4g nuzwf8c ue2xc rdts7wh mnsd8vam uoix0y 98x5nr t9es ofyebf8 xyg4l 4coy8iyay 7gjah3jm opd4pjx1i q8iift 5719 wukk2vd 066odzf2"));
// console.log("!n^^^pp", knockKnock("nfw0fy dnnz4j vfxf 3ee5h6 fyw1spl 3p8sjovc pnoaxhn5a"));
//
// commonCharacter = function(){
//     const ar1 = arguments[0];
//     const ar2 = arguments[1];
//     if(ar1){
//         for(let c in ar1){
//             for(let m in ar2){
//                 if(ar1[c] === ar2[m]){
//                     return ar1[c]
//                 }
//             }
//         }
//     }
// };
//
// commonCharacter(["g", "k", "v"], ["h", "v", "l"]);
//
// arr.forEach(function(part, index, theArray) {
//     theArray[index] = "hello world";
// });
//
// // **********************************  7 kyu: Complementary DNA **********************************
//
// const example = "ATTGC";
//
// function DNAStrand(dna){
//     const side = dna.split("");
//     const otherSide = side.map((a) => {
//         switch (a) {
//             case "A":
//                 return "T";
//             case "T":
//                return "A";
//             case "C":
//                 return "G";
//             case "G":
//                 return "C";
//             default:
//                 console.log("string not accepted")
//         }
//     });
//     return otherSide.join("")
// }
//
// console.log("example: ", DNAStrand(example));
//
// // ***** best practice answer ****
//
// var pairs = {'A':'T','T':'A','C':'G','G':'C'};
//
// function DNAStrand(dna){
//     return dna.split('').map(function(v){ return pairs[v] }).join('');
// }
//
//
// // **********************************  7 kyu: Automorphic Number (Special Numbers Series #6) **********************************
//
// function automorphic(n){
//     const size = n.toString().split("").length;
//     const square = n * n;
//     const nums = square.toString().split("");
//     const lastNum = nums.splice(nums.length - size, size).join("");
//     if(n === parseInt(lastNum)){
//         return "Automorphic"
//     }else{
//         return "Not!!"
//     }
// }
//
// automorphic(25);
//
// // Best Answers
//
// const automorphic = n => String(n*n).endsWith(String(n)) ? "Automorphic" : "Not!!" ;
//
// function automorphic(n){
//     return (n*n+'').slice(-(n+'').length)==n?'Automorphic':'Not!!'
// }
//
// // **********************************  7 kyu: Automorphic Number (Special Numbers Series #6) **********************************
//
// function findOutlier(integers){
//     let first = Math.abs(integers[0]) % 2;
//     let second = Math.abs(integers[1]) % 2;
//     let third = Math.abs(integers[2]) % 2;
//
//     return first + second + third <= 1 ? integers.filter(n => Math.abs(n) % 2 === 1)[0] : integers.filter(n => Math.abs(n) % 2 === 0)[0]
// }
//
// // **********************************  7 kyu: Automorphic Number (Special Numbers Series #6) **********************************
//
// let testArr = [1,2,3,4,3,2,1];
//
// function findEvenIndex(arr){
//     const reducer = (a, b) => a + b;
//     let left = 0;
//     let right = arr.reduce(reducer) - arr[0];
//     for(i = 0; i < arr.length; i++){
//         if(left === right){
//             return i
//         }else{
//             left = left + arr[i];
//             right = right - arr[i + 1];
//         }
//     }
//     return -1
// }
//
// findEvenIndex(testArr);
//
// // **********************************  7 kyu: Find the Divisors **********************************
//
// function divisors(integer) {
//     let arr = [];
//     for (i = integer; i > 0; i--){
//         if(integer % i === 0 && i !=1 && i !=integer) arr = [...arr, i]
//     }
//
//     if(arr.length === 0){ return `${integer} is prime`}else { return arr.sort((a,b) => a - b)}
// };
//
// // **********************************  7 kyu: Alphabet War **********************************

// function alphabetWar(fight) {
//     let split = fight.split("");
//     let leftTotal = 0;
//     let rightTotal = 0
//     split.map(x => {
//        switch (x){
//            case 'w':
//                leftTotal += 4;
//                break;
//            case 'p':
//                leftTotal += 3;
//                break;
//            case 'b':
//                leftTotal += 2;
//                break;
//            case 's':
//                leftTotal += 1;
//                break;
//            case 'm':
//                rightTotal += 4;
//                break;
//            case 'q':
//                rightTotal += 3;
//                break;
//            case 'd':
//                rightTotal += 2;
//                break;
//            case 'z':
//                rightTotal += 1;
//                break;
//            default:
//                console.log("other")
//         }
//     });
//     console.log(split);
//     if(leftTotal > rightTotal){
//         return 'Left side wins!'
//     }else if(leftTotal < rightTotal){
//         return 'Right side wins!'
//     }else{
//         return "Let's fight again!";
//     }
// }
//
// alphabetWar("zdqmwpbs");

// const reinforces =
//     ["g964xxxxxxxx",
//         "myjinxin2015",
//         "steffenvogel",
//         "smile67xxxxx",
//         "giacomosorbi",
//         "freywarxxxxx",
//         "bkaesxxxxxxx",
//         "vadimbxxxxxx",
//         "zozofouchtra",
//         "colbydauphxx" ];
// const airstrikes =
//     ["* *** ** ***",
//         " ** * * * **",
//         " * *** * ***",
//         " **  * * ** ",
//         "* ** *   ***",
//         "***   ",
//         "**",
//         "*",
//         "*" ];

// const reinforces = [ "abcdefg",
//     "hijklmn"];
// const airstrikes = [ "   *   ",
//     "*  *   "];


// console.log(strikeZone(airstrikes[5]));
//
// function air(c, bf, r, a) {
//      console.log("c: ", c);
//      console.log("bf: ", bf);
//      let strikeZone = function(airStrike) {
//         const returnArr = [];
//         const airStrikeArr = airStrike.split('');
//         airStrikeArr.map(function (k, i){
//             if (k === '*'){
//                 returnArr.push(i - 1);
//                 returnArr.push(i);
//                 returnArr.push(i + 1)
//             }
//         });
//         const almostReturn = returnArr.filter((elem, pos, arr) => {
//             return arr.indexOf(elem) === pos;
//         });
//         return almostReturn.filter(i=>i>=0);
//     };
//
//     // console.log("a.length: ", a.length);
//     // console.log("c: ",c);
//
//     if(c === a.length){ return bf }
//     else{
//         let bombArr = [];
//         let reinfArr = [];
//         let airStrike = a[c];
//         let reinforcements = r[c];
//         let strikeZoneArray = strikeZone(airStrike);
//         console.log("strikeZoneArray: ", strikeZoneArray);
//         let battleField = bf.split('');
//         let bombFilter = battleField.map(function (k, i) {
//             if(strikeZoneArray.includes(i)){
//                 bombArr.push("_")
//             }else{
//                 bombArr.push(k)
//             }
//         });
//         console.log("bombFilter: ", bombArr);
//         let reinforcementsFilter = bombArr.map(function (k, i) {
//             if(strikeZoneArray.includes(i)){
//                 reinfArr.push(reinforcements[i])
//             }else{
//                 reinfArr.push(k)
//             }
//         });
//         console.log("reinforcementsFilter: ", reinfArr);
//         return(air(c+1, reinfArr.join(" "), r, a))
//     }
// }
//
// console.log(air(0, reinforces[1], reinforces, airstrikes));

// // **********************************  7 kyu: Mumbling **********************************

// function accum(str) {
//     const s = str.split("");
//     let z = s.map(function(x, i){
//             let rt = x.toUpperCase();
//         for(let j=0; j < i; j++){
//             if(i!==j){
//                 rt = rt.concat(x.toLowerCase())
//             }
//         }
//         return rt
//     });
//     return z.join('-')
// }

// console.log(accum('abcd'));

// function air(c, bf, r, a) {
//     if(c === 1){
//      console.log("c: ", c);
//      console.log("bf: ", bf);
//      console.log("r[c]: ", r[c]);
//     }
//      let strikeZone = function(airStrike) {
//         const returnArr = [];
//         const airStrikeArr = airStrike.split('');
//         airStrikeArr.map(function (k, i){
//             if (k === '*'){
//                 returnArr.push(i - 1);
//                 returnArr.push(i);
//                 returnArr.push(i + 1)
//             }
//         });
//         const almostReturn = returnArr.filter((elem, pos, arr) => {
//             return arr.indexOf(elem) === pos;
//         });
//         return almostReturn.filter(i=>i>=0);
//     };
//     // console.log("a.length: ", a.length);
//     // console.log("c: ",c);
//     if(c === a.length){ return bf }
//     else{
//         let bombArr = [];
//         let reinfArr = [];
//         let airStrike = a[c];
//         let reinforcements = r[c];
//         let strikeZoneArray = strikeZone(airStrike);
//         // console.log("strikeZoneArray: ", strikeZoneArray);
//         let battleField = bf.split('');
//         let bombFilter = battleField.map(function (k, i) {
//             if(strikeZoneArray.includes(i)){
//                 bombArr.push("_")
//             }else{
//                 bombArr.push(k)
//             }
//         });
//         if(c === 1) {
//             console.log("bombFilter: ", bombArr);
//         }
//         let reinforcementsFilter = bombArr.map(function (k, i) {
//             if(strikeZoneArray.includes(i)){
//                 reinfArr.push(reinforcements[i])
//             }else{
//                 reinfArr.push(k)
//             }
//         });
//         // console.log("reinforcementsFilter: ", reinfArr);
//         return(air(c+1, reinfArr.join(""), r, a))
//     }
// }
//
// console.log("Final: ", air(0, reinforces[1], reinforces, airstrikes));
// air(0, reinforces[1], reinforces, airstrikes);

// // **********************************  7 kyu: The wheat/rice and chessboard problem **********************************

//     function squaresNeeded(n) {
//         let t = [1];
//         const reducer = (accumulator, currentValue) => accumulator + currentValue;
//         function multiply(num, c){
//             if(t.reduce(reducer) >= n){
//                 return c + 1
//             }else{
//                 t.push(num * 2);
//                return multiply(num * 2, c + 1)
//             }
//         }
//         return n===0?0: multiply(1, 0)
//     }
//
// console.log("squares", squaresNeeded(0));
// console.log("squares", squaresNeeded(1));
// console.log("squares", squaresNeeded(2));
// console.log("squares", squaresNeeded(3));
// console.log("squares", squaresNeeded(4));

// // **********************************  7 kyu: Vowel Count **********************************

// function vowelCount(str) {
//     let c = 0;
//     const vowles = ["a", "e", "i", "o", "u"];
//     const letters = str.split("");
//     letters.map(l => {
//         if(vowles.includes(l)){
//             c++
//         }
//     });
//     return c
// }
//
// console.log(vowelCount('abracadabra'));

// // **********************************  7 kyu: Two fighters, one winner. **********************************


// class Fighter {
//     constructor(name, health, damagePerAttack) {
//         this.name = name;
//         this.health = health;
//         this.damagePerAttack = damagePerAttack;
//         this.toString = function() { return this.name; }
//     }
// }
//
// function declareWinner(fighter1, fighter2, firstAttacker) {
//     let fa, sa;
//     if(fighter1.name === firstAttacker){
//         fa = fighter1;
//         sa = fighter2
//     }else {
//         fa = fighter2;
//         sa = fighter1
//     }
//
//     function nextAttack(nextFighter, lastFighter) {
//         if(lastFighter.health <= nextFighter.damagePerAttack){
//             return nextFighter.toString()
//         }else{
//             lastFighter.health = lastFighter.health - nextFighter.damagePerAttack;
//            return nextAttack(lastFighter, nextFighter)
//         }
//     }
//     return nextAttack(fa, sa)
// }
//
// console.log("Should be Lew: ", declareWinner(new Fighter("Lew", 10, 2), new Fighter("Harry", 5, 4), "Lew"));
// console.log("Should be Harry: ", declareWinner(new Fighter("Lew", 10, 2), new Fighter("Harry", 5, 4), "Harry"));
// console.log("Should be Harald: ", declareWinner(new Fighter("Harald", 20, 5), new Fighter("Harry", 5, 4), "Harry"));
// console.log("Should be Harald: ", declareWinner(new Fighter("Harald", 20, 5), new Fighter("Harry", 5, 4), "Harald"));
// console.log("Should be Harald: ", declareWinner(new Fighter("Jerry", 30, 3), new Fighter("Harald", 20, 5), "Jerry"));

// // **********************************  7 kyu: Split The Bill. **********************************

// let group = {
//     A: 20,
//     B: 15,
//     C: 10
// };
//
// function splitTheBill(x){
//     const mean = Object.values(x).reduce((a, b)=>a+b)/Object.values(x).length;
//     let returnObj = x;
//     for (let o in returnObj){
//            returnObj[o] = Math.round((returnObj[o] - mean) * 100) / 100
//     }
//     return returnObj
// }
//
// console.log("splitTheBill: ", splitTheBill(group));

// returns {A: 5, B: 0, C: -5}

// // **********************************  7 kyu: Highest and Lowest. **********************************


// function highAndLow(numbers){
//     let arr = numbers.split(" ");
//     let highest = arr[0];
//     let lowest = arr[0];
//     arr.map(x => {
//         if (parseInt(x) >= highest){
//             highest = parseInt(x);
//         }else if (parseInt(x) <= lowest) {
//             lowest = parseInt(x);
//         }
//     });
//     return highest.toString().concat(' ', lowest.toString())
// }

// function highAndLow(numbers){
//     numbers = numbers.split(' ').map(Number);
//     return Math.max.apply(0, numbers) + ' ' + Math.min.apply(0, numbers);
// }
//
// console.log("high: 5, low: 1 = ", highAndLow("1 2 3 4 5")); // return "5 1"
// console.log("high: 5, low: -3 = ", highAndLow("1 2 -3 4 5")); // return "5 -3"
// console.log("high: 9, low: -5 = ", highAndLow("1 9 3 4 -5")); // return "9 -5"
//
//

// // **********************************  7 kyu: Shortest Word. **********************************

// function findShort(s){
//     return Math.min.apply(0, s.split(" ").map(x => x.length))
// }
//
// console.log("3?: ", findShort("bitcoin take over the world maybe who knows perhaps"));
// console.log("3?: ", findShort("turns out random test cases are easier than writing out basic ones"));

// // **********************************  7 kyu: Descending Order. **********************************
// // ******* Not the best answer but I learned about different functions *******

// function descendingOrder(n){
//     return parseInt(n.toString().split("").map(x => parseInt(x)).sort((a, b) => b - a).map(a => a.toString()).join(""))
// }
//
// console.log("decending", (descendingOrder(0)));
// console.log("decending", (descendingOrder(1)));
// console.log("decending", (descendingOrder(123456789)));

// // **********************************  6 kyu: Find the odd int **********************************

// function findOdd(A) {
//     const nums = {};
//      A.map(x => {
//          let string = x.toString();
//        return nums[string] ? nums[string] = nums[string] + 1 : nums[x] = 1
//     });
//     for (let num in nums){
//         if(nums.hasOwnProperty(num)){
//             if(nums[num] % 2 === 1){
//                 return num
//             }
//         }
//     }
// }
//
// console.log("Should be 5: ", findOdd([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]));
// console.log("Should be -1: ", findOdd([1,1,2,-2,5,2,4,4,-1,-2,5]));
// console.log("Should be 5: ", findOdd([20,1,1,2,2,3,3,5,5,4,20,4,5]));
// console.log("Should be 10: ", findOdd([10]));
// console.log("Should be 10: ", findOdd([1,1,1,1,1,1,10,1,1,1,1]));
// console.log("Should be 1: ", findOdd([5,4,3,2,1,5,4,3,2,10,10]));

// // **********************************  6 kyu: Digital Root **********************************

// function digital_root(n) {
//     const root = function (n) {
//         if(n.toString().length === 1) return n;
//         const nums = n.toString().split("").reduce((a,b) => parseInt(a) + parseInt(b));
//         return root(nums)
//     };
//     return root(n)
// }
//
// console.log("Should be 7: ", digital_root(16));
// console.log("Should be 6: ", digital_root(456));

// // **********************************  6 kyu: Digital Root **********************************
//
// function persistence(num) {
//     const root = function (n, c) {
//         if(n.toString().length === 1) return c;
//         const nums = n.toString().split("").reduce((a,b) => parseInt(a) * parseInt(b));
//         return root(nums, c + 1)
//     };
//     return root(num, 0)
// }
//
// console.log("Should be 3: ", persistence(39));
// console.log("Should be 0: ", persistence(4));
// console.log("Should be 2: ", persistence(25));
// console.log("Should be 4: ", persistence(999));

// // **********************************  6 kyu: Stop gninnipS My sdroW! **********************************

// function spinWords(word){
//     return word.split(" ").map(w => w.length >= 5 ? w.split("").reverse().join("") :w).join(" ")
// }
//
// console.log("Hey wollef sroirraw: ", spinWords( "Hey fellow warriors" ));
// console.log("This is a test: ", spinWords( "This is a test"));
// console.log("This is rehtona test: ", spinWords( "This is another test" ));

// // **********************************  5 kyu: Directions Reduction **********************************

//
// function dirReduc(arr){
//     const reduce = function (arr) {
//         let returnArray = arr;
//         returnArray.map((x, i) => {
//             switch (x){
//                 case 'NORTH':
//                     if(returnArray[i + 1] === 'SOUTH'){
//                         returnArray.splice(i, 2);
//                         reduce(returnArray)
//                     }
//                     break;
//                 case 'SOUTH':
//                     if(returnArray[i + 1] === 'NORTH'){
//                         returnArray.splice(i, 2);
//                         reduce(returnArray)
//                     }
//                     break;
//                 case 'EAST':
//                     if(returnArray[i + 1] === 'WEST'){
//                         returnArray.splice(i, 2);
//                         reduce(returnArray)
//                     }
//                     break;
//                 case 'WEST':
//                     if(returnArray[i + 1] === 'EAST'){
//                         returnArray.splice(i, 2);
//                         reduce(returnArray)
//                     }
//             }
//         });
//         return returnArray
//     };
//     return reduce(arr)
// }

// console.log("Should be [\"WEST\"]: ", dirReduc(["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]));
// console.log("Should be [\"NORTH\", \"WEST\", \"SOUTH\", \"EAST\"]: ", dirReduc(["NORTH", "WEST", "SOUTH", "EAST"]));
// console.log("Should be []: ", dirReduc(["NORTH", "SOUTH", "EAST", "WEST", "EAST", "WEST"]));
// console.log("Should be [\\'EAST\\', \\'NORTH\\']: ", dirReduc(['EAST', 'EAST', 'WEST', 'NORTH', 'WEST', 'EAST', 'EAST', 'SOUTH', 'NORTH', 'WEST']));
// console.log("Should be [\\'NORTH\\', \\'EAST\\']: ", dirReduc(['SOUTH', 'NORTH', 'EAST', 'WEST', 'NORTH', 'SOUTH', 'NORTH', 'EAST', 'EAST', 'WEST', 'SOUTH', 'NORTH', 'EAST', 'WEST']));

// // **********************************  5 kyu: Sum of Pairs **********************************

// const sum_pairs = function(ints, s){
//     const map = {};
//     let returnArray = [];
//     ints.forEach((a, i) => {
//         let target = (s - a).toString();
//         if (map[target]){
//             console.log(`item ${a}, (${s} - ${a}) = ${(s - a).toString()}?`);
//             returnArray = [map[(s - a).toString()], a]
//         }else{
//             map[a] = i + 1;
//             // console.log(`item ${a}, ${s} - ${a} = ${(s - a).toString()}`);
//             console.log("map object: ", map);
//         }
//     });
//     return returnArray
// };

// console.log("Should be [1, 7]: ", sum_pairs([1, 4, 8, 7, 3, 15], 8));
// console.log("Should be [0, -6]: ", sum_pairs([1, -2, 3, 0, -6, 1], -6));
// console.log("Should be undefined: ", sum_pairs([20, -13, 40], -7));
// console.log("Should be [1, 1]: ", sum_pairs([1, 2, 3, 4, 1, 0], 2));
// console.log("Should be [3, 7]: ", sum_pairs([10, 5, 2, 3, 7, 5], 10));
// console.log("Should be [4, 4]: ", sum_pairs([4, -2, 3, 3, 4], 8));
// console.log("Should be [0, 0]: ", sum_pairs([0, 2, 0], 0));
// console.log("Should be [13, -3]: ", sum_pairs([5, 9, 13, -3], 10));


// const mapTest = {
//     1: "one",
//     2: "two",
//     3: "three"
// };

// console.log("test: ", mapTest[1]);

// // **********************************  7 kyu: Exes and Ohs **********************************

/**
 * @return {boolean}
 */
// function XO(str) {
//     const x = [];
//     const o = [];
//     const t = [];
//     str.split("").map(item => {
//         item.toUpperCase() === "X" ? x.push(item): t.push(item);
//         item.toUpperCase() === "O" ? o.push(item): t.push(item);
//     });
//     return x.length === o.length
// }
//
// console.log("true: ", XO('xo'));
// console.log("true: ", XO('xxOo'));
// console.log("false: ", XO('xxxm'));
// console.log("false: ", XO('Oo'));
// console.log("false: ", XO('ooom'));

// // **********************************  7 kyu: Disemvowel Trolls **********************************


function disemvowel(str) {
    return str.match(/^[^aeyiuo]+$/)
}

console.log("Ths wbst s fr lsrs LL!: ", disemvowel("This website is for losers LOL!"));





