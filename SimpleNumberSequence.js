let nums = "899091939495";

let missing = function (nums) {
    let findSequence = function (total, count, sp,  countBool, missingArr, aggregateArr) {
        // console.log(`---------------Ran ${total}-----------------`);
        // console.log(`aggregateArr on ${total}`, aggregateArr);
        if(count * (aggregateArr.length) >= nums.length || count >= nums.length){
            return aggregateArr
        }else if(countBool){
            return findSequence(total +1, count, sp + count, true,[], [...aggregateArr, nums.substring(sp, sp + count)] )
        }else if(nums.substring(sp, count) - nums.substring(sp + count, sp+count+count) !== -1){
            return findSequence(total +1, count === 0  ? count +1 : count+1, sp, false, [], [])
        }else if(nums.substring(sp, count) - nums.substring(sp + count, sp+count+count) === -1){
            return findSequence(total +1, count, count, true, missingArr, [...aggregateArr, nums.substring(sp, count)] )
        }
    };
    const numsArr = findSequence(0, 0, 0, false, [], []).map(n => parseInt(n));
    console.log("numsArr:", numsArr);
    for(let i = 0; i < numsArr.length; i++){
        if(numsArr[i+1] - numsArr[i] !== 1 ){
        console.log("here: ", numsArr[i]);
            return (numsArr[i] +1)
        }
    }
};

missing(nums);




// let nums = "10111213141617";
// function missing(s, digits = 1) {
//     const numsToArr = [];
//     for (let i = 0; i < s.length; i += digits) {
//         numsToArr.push(parseInt(s.substring(i, i + digits), 10));
//     }
//     const constructedArr = Array.from(new Array(numsToArr.length)).map((a, ind) => parseInt(s.substring(0, digits)) + ind);
//     const percentageTheSame = (numsToArr.filter(a => constructedArr.includes(parseInt(a, 10))).length / constructedArr.length) * 100;
//     if (percentageTheSame >= 70) {
//         return constructedArr.find(a => !numsToArr.includes(a))
//     }
//     missing(s, ++digits);
// }
//
// nums.substring(sp, count) - nums.substring(sp + count, sp+count+count)
//
// nums.substring(sp, count) - nums.substring(sp + count, sp+count+count)





// let missing = function (nums) {
//     let findSequence = function (total, count, sp,  countBool, missingArr, aggregateArr) {
//         console.log(`---------------Ran ${total}-----------------`);
//         console.log(`aggregateArr on ${total}`, aggregateArr);
//         if(count * (aggregateArr.length) >= nums.length || count >= nums.length){
//             return aggregateArr
//         }else if(countBool){
//             console.log("stage 1: aggragateArr ", aggregateArr);
//             console.log("stage 2:sp ", sp);
//             console.log("stage 2:count ", count);
//             console.log("stage 2:substring 1 ", nums.substring(sp, count + count));
//             console.log("stage 2:substring 2 ", nums.substring(sp + count, sp+count+count));
//             console.log("aggreateArr should be: ", [...aggregateArr, nums.substring(sp, sp + count)]);
//             return findSequence(total +1, count, sp + count, true,[], [...aggregateArr, nums.substring(sp, sp + count)] )
//         }else if(nums.substring(sp, count) - nums.substring(sp + count, sp+count+count) !== -1){
//             console.log("stage 2:count ", count);
//             console.log("stage 2:sp ", sp);
//             console.log("stage 2:substring 1 ", nums.substring(sp, count));
//             console.log("stage 2:substring 2 ", nums.substring(sp + count, sp+count+count));
//             return findSequence(total +1, count === 0  ? count +1 : count+1, sp, false, [], [])
//         }else if(nums.substring(sp, count) - nums.substring(sp + count, sp+count+count) === -1){
//             console.log("stage 3 aggregateArr: ", aggregateArr);
//             console.log("stage 3 count: ", count);
//             console.log("stage 3 sp: ", sp);
//             console.log("stage 3:substring 1 ", nums.substring(sp, count));
//             console.log("stage 3:substring 2 ", nums.substring(sp + count, sp+count+count));
//             return findSequence(total +1, count, count, true, missingArr, [...aggregateArr, nums.substring(sp, count)] )
//         }
//     };
//     return findSequence(0, 0, 0, false, [], [])
// };
//
// missing(nums);


function missing(s, digits = 1) {
    const numsToArr = [];
    for (let i = 0; i < s.length; i += digits) {
        numsToArr.push(parseInt(s.substring(i, i + digits), 10));
    }
    const constructedArr = Array.from(new Array(numsToArr.length)).map((a, ind) => {
        return parseInt(s.substring(0, digits)) + ind
    });
    const numsArrToSingle = numsToArr.join('').split('')
    const constructedArrToSingle = constructedArr.join('').split('')
    const percentageTheSame = (numsArrToSingle.filter(a => constructedArrToSingle.includes(a)).length / constructedArrToSingle.length) * 100;
    if (percentageTheSame >= 60) {
        return constructedArr.find(a => !numsToArr.includes(a))
    }
    missing(s, ++digits);
}